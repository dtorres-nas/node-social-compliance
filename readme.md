# NAS ACTIVATE Social Compliance

A Node.js terminal program that is designed to scan the local or web ACTIVATE file repository and determine compliance of HTML documents based on Social Media and Meta Requirements.

This script ultimately generates a ```.csv``` file based on the presence of ```<meta>``` tags and/or ```og-logo.jpg``` files for each tenant in our repository..

---

## Social Compliance Criteria

Our sites require the following traits in order to comply with our social and SEO requirements:

1. A file named ```og-logo.jpg``` must be present in the ```Web/Content/{TenantShortName}/Images/``` folder
2. All files in the ```Web/Views/{TenantShortName}/Index/``` and ```Web/Views/{TenantShortName}/Creative/``` must include the following HTML:
    1. ```<meta>``` tag with the attribute ```name``` that includes ```"description"```
    2. ```<meta>``` tag with the attribute ```name``` that includes ```"twitter:"``` **or** ```<meta>``` tag with the attribute ```property``` that includes ```"og:"```

This results in 7 compliance levels possible, they are named accordingly:

1. **Full Compliance**  
   *(og-logo present, all files have meta page and social tags)*
2. **Social Tags and OG-Logo Only**  
  *(no page tags present)*
3. **Page Tags and OG-Logo Only**  
  *(no social tags present)*
4. **Social Tags Only**  
  *(no og-logo or page tags present)*
5. **Page Tags Only**  
  *(no og-logo or social tags present)*
6. **OG-Logo Only**  
  *(no meta tags present)*
7. **None**  

---

## How to Use this Tool

Technical Requirements:

- ```Node.js```
- ```git```
- ```terminal```
- ```ACTIVATE Repo Access```

### Getting Started

Clone this repo with the following code:

```
git clone https://{user}@bitbucket.org/dtorres-nas/node-social-compliance.git
cd node-social-compliance
npm install
```

Before you run this script, make sure your local ACTIVATE instance is up to date for accurate data:
```
cd ../path/to/activate
git pull
cd ../path/to/node-social-compliance
```

Edit ```index.js``` in your local directory structure to target Activate folders:

```js
// Change the relative paths as needed to reach from this repo to the "activate" repository
const ActivateViewsPath = "../path/to/activate/Web/Views/";
const ActivateContentPath = "../path/to/activate/Web/Content/";
// An example local path is "../../../source/repos/activate/..."
```

---

### Running the Script

To run this script you call ```node index.js``` to run a scan based on the **beta** version of Activate files.  If you only want to scan your local directory, you can run ```node index-byfile.js``` instead.  **Note:** running local files can give you false-negative results for clients that are using partial views or other programmatic methods of adding social media tags.


For standard scans, this script can take some time as each page's beta HTML gets downloaded and parsed.  When you start a scan you'll see a progress bar showing which tenant is being processed, as well as the current overall progress:

```
node index.js
Starting compliance scan, looking at beta site HTML code...
[████████████████████████████████████████] 100% | ETA: 0s | OUMI | 95/95
Compliance scan complete!
Open ./nas-activate_social-compliance_2022-02-28_132029.csv to see your results
```

For local scans, the process is much faster and will not output a progress bar, instead it will read like this:

```
node index-byfile.js
Starting compliance scan, looking at local CSHTML code...
Compliance scan complete!
Open ./nas-activate_social-compliance_by-file_2022-02-28_132029.csv to see your results
```

The new file will be called ```nas-activate_social-compliance_{yyyy-mm-dd}_{hhmmss}.csv```. Opening this file in Excel or another spreadsheet application, you get something like this:

| Tenant Shortname   | Social Compliance            |
| ------------------ | ---------------------------- |
| TenantName_A       | Page Tags and OG-Logo Only   |
| TenantName_B       | Social Tags and OG-Logo Only |
| ...                | ...                          |

Now you've got a full list of all tenants and their compliance status to be shared with the team.

## Future

This script can be expanded in a few ways:

* Check for more detail in the extent of compliance (for example, checks if ```<meta description="">``` exists, but does not check if ```"description"``` is empty or not)  
* Produce detailed lists of current ```<meta>``` tags used on each page, per each tenant.  
* Check for production compliance instead (possible issues with conflict between beta and production, script would need an alternate version that handles exceptions)  
* Branch script could be made for accessibility compliance instead, such as image ```alt``` tags  
* Automation possible, could be set up on a cron job to run and automatically email results periodically  