const fs = require("fs");
const htmlToJson = require("html-to-json");
const ObjectsToCsv = require("objects-to-csv");
const FoldersFilesToExclude = require("./foldersToExclude");
const TenantLiveURL = require("./tenantLiveURL");
const cliProgress = require('cli-progress');

const fileProgressBar = new cliProgress.SingleBar({
  format: '[{bar}] {percentage}% | ETA: {eta}s | {value}/{total} | {tenantName} | {fileName}'
}, cliProgress.Presets.shades_classic);

async function main() {
  console.log("Starting compliance scan, looking at beta site HTML code...");
  // Create local views path and list of non-Tenant folders
  const ActivateViewsPath = "../../../source/repos/activate/Web/Views/";
  const ActivateContentPath = "../../../source/repos/activate/Web/Content/";
  // Scan All Tenants in Views folder and Create List of Tenants
  const filteredFolders = fs
    .readdirSync(ActivateViewsPath)
    .filter((TenantShortname) => !FoldersFilesToExclude.includes(TenantShortname));
    
  const numberOfTenants = filteredFolders.length;
  // Create async array of all view tenant View folders
  const folderData = filteredFolders.map(async (TenantShortname) => {
    if (TenantLiveURL(TenantShortname) === undefined) {
      console.log(`\nProcess terminated.\n${TenantShortname} is missing a URL declaration in tenantLiveURL.js`);
      process.exit(1);
    }
    // Create Path Strings
    const tenantContentPath = `${ActivateContentPath}${TenantShortname}`;
    const tenantViewPath = `${ActivateViewsPath}${TenantShortname}`;

    // Check for og-logo compliance
    const tenantHasOGLogo = fs.existsSync(
      `${tenantContentPath}/Images/og-logo.jpg`
    );

    // Create array of View folders, excluding "Shared"
    const filteredViewFolders = fs
      .readdirSync(tenantViewPath)
      .filter((tenantViewFolder) => tenantViewFolder !== "Shared");


    // Create async array of file data in the View folders
    const tenantViewData = filteredViewFolders.map(async (tenantViewFolder) => {
      fileProgressBar.start(numberOfTenants, 0, {tenantName: "...", fileName: "..."});
      // Build file path for parsing
      const tenantFilePath = `${ActivateViewsPath}${TenantShortname}/${tenantViewFolder}`;
      // Create array of file name and Web URL data
      const tenantWebData = fs.readdirSync(tenantFilePath).map((fileName) => {
        const strippedFileName = fileName.substring(0, fileName.lastIndexOf("."));
        const webRoot = `http://${TenantLiveURL(TenantShortname)}.nasbeta.com/`;
        const webPath = tenantViewFolder !== "Home" ? `creative/${strippedFileName}` : ``;
        return {
          fileName,
          webPath: `${webRoot}${webPath}`,
        };
      });
      // Async parse utility function, returns <meta> tag information and fileName
      const parseWebFile = async ({ webPath, fileName }) => {
        const metaTags = await htmlToJson.request(webPath, function () {
          return this.map("meta", function ($item) {
            return $item.attr();
          });
        });
        fileProgressBar.update({fileName: fileName});
        return {
          fileName,
          metaTags: metaTags !== undefined ? metaTags : [],
        };
      };
      // Resolve array of Promises returned from mapped async Parse utility
      const parseAllWebFiles = async () => {
        return Promise.all(tenantWebData.map((file) => parseWebFile(file)));
      };
      // Create async array of file data in the View folders
      const parsedFolderWebData = await parseAllWebFiles().then((fileData) => {
        if (fileData.length === 0) {
          // Skip step if no tags found in parseAllFiles()
          return {
            tenantViewFolder,
          };
        } else {
          // Create new array of tags found in fileData
          const pageTags = fileData.map((file) => {
            const { metaTags, fileName } = file;
            if (metaTags) {
              // Extract just the Page tags
              const justPageTags = metaTags.filter((tag) => {
                if (tag.name) {
                  return tag.name === "description";
                }
              });
              // Extract just the Social tags
              const justSocialTags = metaTags.filter((tag) => {
                if (tag.property) {
                  return tag.property.includes("og:");
                } else if (tag.name) {
                  return tag.name.includes("twitter:");
                }
              });
              // Create function to check compliance based on extracted tags
              const fileCompliance = (pageTagList, socialTagList) => {
                if (pageTagList.length === 0 && socialTagList.length === 0) {
                  return "None";
                } else if (pageTagList.length > 0 && socialTagList.length === 0) {
                  return "Page Tags Only";
                } else if (pageTagList.length === 0 && socialTagList.length > 0) {
                  return "Social Tags Only";
                } else if (pageTagList.length > 0 && socialTagList.length > 0) {
                  return "Full";
                } else {
                  return "None";
                }
              };
              // Return object based on file details
              return {
                fileName,
                justPageTags,
                justSocialTags,
                fileCompliance: fileCompliance(justPageTags, justSocialTags),
              };
            } else {
              // Return object based on file details
              return {
                fileName,
                justPageTags: [],
                justSocialTags: [],
                fileCompliance: "None",
              };
            }
          });
          // Return name of folder and array of page tag data
          return {
            tenantViewFolder,
            pageTags,
          };
        }
      });

      return { parsedFolderWebData };
    });

    // Create async function to map through all tenants and get file data
    const parseTenantViewWebData = async () => {
      return Promise.all(tenantViewData.map(async (viewFolder) => {
        const data = await viewFolder;
        return data.parsedFolderWebData;
      }));
    };
    // Resolve tenant data Promise
    const tenantViewWebDataParsed = await parseTenantViewWebData().then(
      (data) => {
        fileProgressBar.increment({tenantName: TenantShortname});
        return data
      }
    );
    // Create function to check for and return compliance as String
    const tenantCompliance = (hasOGLogo, viewData) => {
      const foldersWithTags = viewData.filter((folder) => {
        const { pageTags } = folder;
        if (pageTags === undefined) {
          return false;
        } else {
          const tagList = pageTags.filter(
            (page) => page.fileCompliance !== "None"
          );
          return tagList.length > 0;
        }
      });
      const tenantHasTags = foldersWithTags.length > 0;
      // Function checks data arrays and compares number of compliant
      // files with total number of files
      const isTenantCompliant = (fileComplianceType) => {
        const mergedPageData = [];
        foldersWithTags.forEach((folder) => {
          folder.pageTags.forEach((tag) => {
            mergedPageData.push(tag);
          });
        });
        const compliantFiles = mergedPageData.filter((page) => {
          return page.fileCompliance === fileComplianceType;
        });
        return mergedPageData.length === compliantFiles.length;
      };


      // Return compliance based on logic booleans
      if (!hasOGLogo && !tenantHasTags) {

        return "None";

      } else if (hasOGLogo && !tenantHasTags) {

        return "OG-Logo Only";

      } else if (!hasOGLogo && tenantHasTags) {

        if (isTenantCompliant("Full")) {

          return "Social and Page Tags Only";

        } else if (isTenantCompliant("Social Tags Only")) {

          return "Social Tags Only";

        } else {

          return "Page Tags Only";

        }

      } else if (hasOGLogo && tenantHasTags) {

        if (isTenantCompliant("Full")) {

          return "Full Compliance";

        } else if (isTenantCompliant("Social Tags Only")) {

          return "Social Tags and OG-Logo Only";

        } else {

          return "Page Tags and OG-Logo Only";

        }

      }
    };
    return {
      "Tenant Shortname": TenantShortname,
      "Social Compliance": tenantCompliance(
        tenantHasOGLogo,
        tenantViewWebDataParsed
      ),
    };
  });
  
  // Resolve all View folder data
  const allParsedData = await Promise.all(folderData);

  // Create function to sort list by compliance level
  function sortResultsCompliance(x, y) {
    if (x["Social Compliance"] < y["Social Compliance"]) { return -1; }
    if (x["Social Compliance"] > y["Social Compliance"]) { return 1; }
    return 0;
  };

  const sortedResultsByCompliance = allParsedData.sort(sortResultsCompliance);

  // Create new CSV based on results object
  const csv = new ObjectsToCsv(sortedResultsByCompliance);
  const currentDate = new Date();
  const calendarDate = currentDate.toJSON().slice(0, 10);
  const currentTime = `${currentDate.getHours()}${currentDate.getMinutes()}${currentDate.getSeconds()}`;
  const csvFileName = `./nas-activate_social-compliance_${calendarDate}_${currentTime}.csv`;
  await csv.toDisk(csvFileName);

  // Stop progress bar
  fileProgressBar.update({tenantName: "...", fileName: "..."});
  fileProgressBar.stop();

  // Return final results object for visibility in terminal
  console.log("Compliance scan complete!");
  console.log(`Open ${csvFileName} to see your results`);
};

main().then().catch((ex) => console.log(ex.message));